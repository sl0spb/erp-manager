<?php

declare(strict_types=1);

namespace App\Model\Work\UseCase\Projects\Task\Create;

use App\Model\Work\Entity\Projects\Task\Type;
use Symfony\Component\Validator\Constraints as Assert;

class Command
{
    /**
     * @var string
     * @Assert\NotBlank()
     */
    public string $project;
    /**
     * @var string
     * @Assert\NotBlank()
     */
    public string $member;
    /**
     * @var NameRow[]
     * @Assert\NotBlank()
     * @Assert\Valid()
     */
    public array $names = [];
    /**
     * @var string
     */
    public ?string $content = null;
    /**
     * @var string|null
     */
    public ?string $parent = null;
    /**
     * @var \DateTimeImmutable
     * @Assert\Date()
     */
    public ?\DateTimeImmutable $plan = null;
    /**
     * @var string
     * @Assert\NotBlank()
     */
    public string $type;
    /**
     * @Assert\NotBlank()
     */
    public int $priority;

    public function __construct(string $project, string $member)
    {
        $this->project = $project;
        $this->member = $member;
        $this->type = Type::NONE;
        $this->priority = 2;
    }
}